# Overview

Example of operational use of the RSC template and gitlab's CI to build singularity containers for your project.
Typically triggered by a tag push.


## Initial setup:

To develope you project we suggest using a virtual environment.

```bash
# Specify project name and author
PROJECT_NAME=rsc_demo
AUTHOR='Your Name'

# Create the virtual environment
python3 -m venv ~/venvs/${PROJECT_NAME}

# Activate the virtual environment
source ~/venvs/${PROJECT_NAME}/bin/activate
pip install pyscaffold
pip install cookiecutter
```

A new RSC templated project can be created by issuing the following commands:


```bash


# Create the package structure with pyscaffold
putup ${PROJECT_NAME} --no-skeleton

# Add rsc related defaults to the package
cookiecutter --no-input -f https://gitlab.com/rscdata_science/system/rsc_template \
         project_name=${PROJECT_NAME} \
         author='${AUTHOR}'
```
**_NOTE:_**  See [putup documentation](https://github.com/pyscaffold/pyscaffold/) and [cookiecutter](https://cookiecutter.readthedocs.io)

```bash
# Install requirements
pip install -r ${PROJECT_NAME}/requirements.txt
pip install -r ${PROJECT_NAME}/requirements_rsc.txt

# Install the package in edition mode (-e)
pip install -e ${PROJECT_NAME}
```

**_NOTE:_**  See [pip documentation](https://pip.pypa.io/en/stable/) and [venv documentation](https://docs.python.org/3.6/library/venv.html)


To track the project in gitlab, **you first need to create a new gitlab project in gitlab.com** and the push the project to the repo.

```bash
cd ${PROJECT_NAME}
git remote add origin https://gitlab.com/rscdata_science/system/${PROJECT_NAME}
git push -u origin --all
git push -u origin --tags
```

Then you can open the `${PROJECT_NAME}` folder in your favorite IDE and start coding.

The design of the package encourages the separation of the functions you want to apply to the remote sensing data
(`${PROJECT_NAME}\src\${PROJECT_NAME}\utils.py:my_function`) from the implementation scripts (`${PROJECT_NAME}\src\${PROJECT_NAME}\rsc_operational_code\process_image.py:run`).
This design should simplify the implementation of the project in a different
compute environment.


## How to run it
### From virtualenv

If all the required software is available on ahtena, you could run it in the virtual environment just like:

```bash
process_image -v DEBUG\
cemsre_t55jgm_20180423_abam5.img \
cemsre_t55jgm_20180423_zzym5.img \
--n 1

> 2020-02-12 13:17:39 athena52 rsc_demo.rsc_operational_code.process_image[68963] DEBUG Using version: 0.0.2+g6c956eb.dirty
> 2020-02-12 13:17:39 athena52 rsc_demo.rsc_operational_code.process_image[68963] INFO Starting useful calculations.
> 2020-02-12 13:18:01 athena52 rsc_demo.rsc_operational_code.process_image[68963] DEBUG Adding metadata to the new image
> 2020-02-12 13:18:01 athena52 rsc_demo.rsc_operational_code.process_image[68963] INFO Script ends here

```

### From container

A new version of the container is created every time a commit gets pushed.

Note: Don't forget to configure your USERNAME and PASSWORD in gitlab->settings->CI/CD >Variables.

```bash
git add .
VERSION=0.1.0
git commit -a -m 'Initial release'
git push
git tag -a $VERSION -m 'Initial release'
git push origin $VERSION
```
The idea is to have our singularity hub instance running on athena to pull the images from. In the meantime, the images need to be pulled from gitlab like:

```bash
singularity pull --docker-username $USERNAME --docker-password $PASSWORD oras://registry.gitlab.com/rscdata_science/${PROJECT_NAME}:$VERSION
```

Once the image is available locally, the scripts defined as entry points in config.cfg can be executed as follows:

```bash
./${PROJECT_NAME}_$VERSION.sif process_image \
      cemsre_t55jgm_20180423_abam5.img \
      cemsre_t55jgm_20180423_zzym5.img \
      --n 1
```

## Documentation

We use gitlab pages to serve sphinx documentation. If the project follows shpinx docstring rules, a documentation site will be built when pushing a new tag.
See https://rscdata_science.gitlab.io/cicd_demo/.

**_NOTE:_**  See [sphinx documentation](https://www.sphinx-doc.org/en/master/)
