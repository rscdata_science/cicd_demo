"""
Documentation for apply with rios
"""

from rios import applier
import rscutils.history as history
import cicd_demo
from cicd_demo.utils import my_function
import sys
import click
import click_log
import logging, coloredlogs


__author__ = "${author}"
__copyright__ = "${author}"
__license__ = "${license}"
__version__  = cicd_demo.__version__

#Start the logger
logger = logging.getLogger(__name__)


@click.command()
@click.option('-i','--in_image',
                help='Input image',
                required=True,
                type=click.Path(exists=True))
@click.option('-o','--out_image',
                help='Output image',
                required=True,
                type=click.Path(exists=False))
@click.option('-n',
                help='number to add to the image',
                required=True)

@click.version_option()
@click_log.simple_verbosity_option(logger)
def run(in_image, out_image,n):
    """A really useful function.

    Returns None

    """
    # Add nice formatting to the logger
    coloredlogs.install(logger=logger, level=logger.level)
    logger.debug(f"Using version: {__version__}")
    logger.info("Starting useful calculations.")

    # Set up input and output filenames.
    infiles = applier.FilenameAssociations()
    infiles.in_image = in_image

    outfiles = applier.FilenameAssociations()
    outfiles.out_image = out_image

    otherargs = applier.OtherInputs()
    otherargs.n = int(n)

    # Set up the function to be applied
    def model(info, inputs, outputs, otherargs):
        """
        Function to be called by rios.
        """
        outputs.out_image = my_function(inputs.in_image, otherargs.n)


    # Apply the function to the inputs, creating the outputs.
    applier.apply(model, infiles, outfiles, otherargs)
    opt_dict = {}
    logger.debug("Adding metadata to the new image")
    history.insertMetadataFilename(str(out_image), [str(in_image)], opt_dict, None, None)
    logger.info("Script ends here")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)

if __name__ == '__main__':
    run()
