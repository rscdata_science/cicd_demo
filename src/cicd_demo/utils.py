import numpy as np

def my_function(array, n):
    '''
    Sample function that simply adds n to an array

    Args:
        array (np.array)
        n (number)

    Returns:
        np.array: array
    '''

    return array * n
